const { getPostchainConn, getSignerFromPrivKey } = require('./postchain');

/**
 * SEND TX to postchain
 * 
 * opName: string - name of the operation to be called
 * args: string - arguments of the operations, need to be converted into values
 * privKey: string | null - private key of the account that needs to sign the tx (can be null)
 * nodeUrl: string - endpoint where to reach the postchain node
 * brid: string - the BRID of the chain in hex string
 * 
 * nop: boolean - if a nop is required (against replay attack)
 */
async function transaction(opName, args, privKey, nodeUrl = "http://localhost:7740", brid, nop = false) {

  const gtx = getPostchainConn(nodeUrl, brid);

  const signers = []; // in future, we could support multiple signers
  
  if(privKey) {
    const signer = getSignerFromPrivKey(privKey);
    signers.push(signer);
  }

  args = convertParams(args);

  let tx = gtx.newTransaction(signers.map(signer => signer.pubKey));
  tx.addOperation(opName, ...args);
  if(nop) tx.addOperation("nop", Date.now());
  signers.map(signer => tx.sign(signer.privKey, signer.pubKey))

  print("Sending...");
  return signers[0]? tx.postAndWaitConfirmation(signers[0].privKey, signers[0].pubKey) : tx.postAndWaitConfirmation()
}

function convertParams(args) {
  return args.map(arg => {
    switch(true) {
      case(/int:\d*/.test(arg)):
        return parseInt(arg.substr(arg.indexOf(":") + 1));
      case(/byte:[[:xdigit:]]*/.test(arg)): // hex digits
        return Buffer.from(arg.substr(argsindexOf(":") + 1), 'hex');
      default:
        return arg;
    }
  })
}

module.exports = transaction;