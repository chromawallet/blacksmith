const pcl = require('postchain-client');

/**
 * SEND QUERY to postchain
 * 
 * queryName: string - name of the query to be called
 * queryParams: obj (json) -  paramaters of the query string
 * nodeUrl: string - endpoint where to reach the postchain node
 * brid: string - the BRID of the chain in hex string
 */

async function query(queryName, queryParams, nodeUrl = "http://localhost:7740", brid) {
  const node_api_url = nodeUrl; // using default postchain node REST API port

  const rest = pcl.restClient.createRestClient(node_api_url, brid, 5);
  const gtx = pcl.gtxClient.createClient(
    rest,
    Buffer.from(brid, 'hex'),
    [] );
    

  const params = queryParams || {}

  return gtx.query(queryName, params);
}

module.exports = query;