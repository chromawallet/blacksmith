const pcl = require('postchain-client');

function getPostchainConn(nodeUrl, brid) {
  const node_api_url = nodeUrl; // using default postchain node REST API port

  const rest = pcl.restClient.createRestClient(node_api_url, brid, 5);
  const gtx = pcl.gtxClient.createClient(
    rest,
    Buffer.from(brid, 'hex'),
    [] );
  return gtx;
}

function getSignerFromPrivKey(privKey) {
  const signer = {
    privKey: Buffer.from(privKey, 'hex'),
    pubKey: new pcl.util.createPublicKey(Buffer.from(privKey, 'hex'))
  }
  return signer;
}

module.exports = {
  getPostchainConn,
  getSignerFromPrivKey
};