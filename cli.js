#! /usr/bin/env node

const yargs = require('yargs');
const query = require('./query');
const transaction = require('./transaction');

print = console.log;
printSuccess = (...output) => {
  print("\x1b[32m", ...output, "\x1b[0m");
}
console.log = function() {}

var commandHandlers = yargs.getCommandInstance().getCommands();
var argv = yargs
      .usage('Usage: $0 <command>')
      .command('tx <opName>', "Send one transaction to postchain", function(yargs) {
          yargs.usage("Usage: $0 tx [options]")
            .example('$0 tx insert_city --args "Cesenatico" --brid 003800CB90FB492012940F712A821A011B5BE4F07DE414665893B59EF110291A')
            .example("$0 tx insert_city --args Bologna --brid 003800CB90FB492012940F712A821A011B5BE4F07DE414665893B59EF110291A  -u http://localhost:7740 -k a30d99841004de1133d8b40ff01a50bbaa6ed944e0794d5bf52052696e214fbc --nop")

          argsParams(yargs, false)
          privKeyParams(yargs, false)
          bridParam(yargs)
          nodeUrlParam(yargs, false)
          nopParam(yargs, false)
          
          // yargs.middleware()
      }, async (argv) => {
        try {
          await transaction(argv.opName, argv.args, argv.priv, argv.nodeUrl, argv.brid, argv.nop)
          printSuccess("Confirmed");
        } catch(e) {
          print("\x1b[31m", "Error", "\x1b[0m");
          print(e);
        }
      })
      
      .command('query <queryName>', "Send one query to postchain", function(yargs) {
        yargs.usage("Usage: $0 query [options]")
          .example("$0 query get_cities --brid 003800CB90FB492012940F712A821A011B5BE4F07DE414665893B59EF110291A")
          .example("")

          argsParams(yargs, false)
          bridParam(yargs)
          nodeUrlParam(yargs, false)
      }, async (argv) => {
        try {
          const res = await query(argv.queryName, null, argv.nodeUrl, argv.brid)
          printSuccess("QueryResult", res);
        } catch(e) {
          print("\x1b[31m", "Error", "\x1b[0m");
          print(e);
        }
      })
      .demandCommand().recommendCommands().strict()
      .alias("h", "help")
      .help('help')
      .argv

function privKeyParams(yargs, required = true) {
  yargs.options({
    "k": {
      alias: "priv",
      describe: "(hex) private key of the account that needs to sign the tx (can be null)",
      type: "string"
    }
  });
  if(required) yargs.demandOption("k")
}

function argsParams(yargs, required = true) {
  yargs.options({
    "a": {
      alias: "args",
      describe: "Arguments of the operations, need to be converted into values",
      type: "array"
    }
  })
  
  if(required) yargs.demandOption("a")
}

function bridParam(yargs, required = true) {
  yargs.options({
    'b': {
      alias: 'brid',
      describe: 'Blockchain RID of the module',
      type: 'string'
    }
  })
  .nargs('b', 1)

  if(required) yargs.demandOption("b")
}

function nodeUrlParam(yargs, required = true) {
  yargs.options({
    "u": {
      alias: "nodeUrl",
      describe: "Endpoint where to reach the postchain node",
      default: "http://localhost:7740",
      type: "string"
    }
  })
  .nargs("u", 1)

  if(required) yargs.demandOption('u')
}

function nopParam(yargs, required = true) {
  yargs.option({
    "n": {
      alias: "nop",
      describe: "Set nop if required (against replay attack)",
      type: "boolean"
    }
  })

  if(required) yargs.demandOption('n')
}